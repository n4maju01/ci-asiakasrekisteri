# CI Asiakasrekisteri #

http://pitkamatkaeipelota.fi/ci_asiakasrekisteri/

### FI ###

PHP:llä ohjelmoitu asiakasrekisteri, joka käyttää CodeIgniter- ja Bootstrap -frameworkeja. Tällä hetkellä sisältää asiakkaiden lisäämisen, muokkaamisen, poistamisen, haun ja sivutuksen.

### EN ###

A customer registry application coded with PHP using CodeIgniter and Bootstrap frameworks. At the moment you can add, update, delete and search customers and also features pagination.