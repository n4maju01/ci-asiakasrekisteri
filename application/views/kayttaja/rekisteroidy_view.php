<div class="row">
    <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2">
        <h3>Rekisteröidy asiakarekisteriin</h3>
        <form role="form" method="post" action="<?php echo site_url() . 'kayttaja/tallenna' ?>">
            <div class="form-group">
               <label for="email">Sähköposti:</label>
                <input type="text" class="form-control" id="email" name="email"
                    maxlength="100" value="<?php echo $email; ?>">
                <?php echo form_error('email'); ?>
            </div>
            <div class="form-group">
               <label for="email">Salasana:</label>
               <input type="password" class="form-control" id="salasana" name="salasana"
                    maxlength="25" value="<?php echo $salasana; ?>">
                <?php echo form_error('salasana'); ?>
            </div>
            <a class="btn btn-danger" href="<?php echo site_url(); ?>">Takaisin</a>
            <button type="submit" class="btn btn-success">Rekisteröidy</button>
        </form>
    </div>
</div>