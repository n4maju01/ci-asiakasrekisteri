<h3><?php echo $otsikko; ?></h3>
<form id="etsi_lomake" method="post" action="<?php echo site_url() . "asiakas/index"; ?>">
    <input id="etsi" name="etsi" type="hidden">
</form>
<form method="post" action="<?php echo site_url() . "asiakas/suorita"; ?>">
<div class="row">
    <div class="col-md-6">
        <div class="form-group has-feedback">
            <label for="search" class="sr-only">Search</label>
            <input type="text" class="form-control" name="search" id="search"
                   placeholder="Kirjoita hakusana ja paina enter...">
            <span class="glyphicon glyphicon-search form-control-feedback"></span>
            <?php 
            echo "<a class='btn btn-xs btn-default' href=" . site_url() .
                'asiakas/index' . ">$etsi<span class='glyphicon glyphicon-remove'</span></a>";
            ?>
        </div>
    </div>
    <div class="col-md-2">
        <a href="<?php echo site_url() . 'asiakas/lisaa'?>" 
            class="btn btn-primary">Lisää uusi</a>
    </div>
    <div class="col-md-2 col-md-offset-2">
        <input type="submit" name="poista" value="Poista" class="pull-right btn btn-success">
    </div>
</div>
<br>
<div class="row">
    <div class="col-lg-8 col-lg-offset-2">
        <table class="table table-striped">
            <tr>
                <th><?php echo anchor("asiakas/index/$sivutuksen_kohta/etunimi", "Etunimi"); ?></th>
                <th><?php echo anchor("asiakas/index/$sivutuksen_kohta/sukunimi", "Sukunimi"); ?></th>
                <th><?php echo anchor("asiakas/index/$sivutuksen_kohta/email", "Sähköposti"); ?></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        <?php 
        foreach ($asiakkaat as $asiakas) {
            echo "<tr>";
            echo "<td>$asiakas->etunimi</td>";
            echo "<td>$asiakas->sukunimi</td>";
            echo "<td>$asiakas->email</td>";
            echo "<td>" . anchor("asiakas/muokkaa/$asiakas->id", "<span class='glyphicon glyphicon-edit'></span>") . "</td>";
            echo "<td>" . anchor("toimenpide/$asiakas->id", "<span class='glyphicon glyphicon-pencil'></span>") . "</td>";
            echo "<td>" . anchor("asiakas/varmistus/$asiakas->id", "<span class='glyphicon glyphicon-trash'></span>") . "</td>";
            echo "<td><input type='checkbox' name='valinta[]' value='$asiakas->id'></td>";
            echo "</tr>";
        }
        ?>
        </table>
        <?php 
        echo $this->pagination->create_links();
        ?>
    </div>
</div>
</form>