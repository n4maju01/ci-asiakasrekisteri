<h3><?php echo $otsikko; ?></h3>
<?php 
//echo validation_errors();
?>
<form role="form" method="post" action="<?php echo site_url() . 'asiakas/tallenna' ?>">
    <input type="hidden" id="id" name="id" value="<?php echo $id; ?>">
    <div class="form-group">    
        <label for="etunimi">Etunimi:</label>
        <input type="text" class="form-control" id="etunimi" name="etunimi" 
               maxlength="50" value="<?php echo $etunimi; ?>" autofocus>
        <?php echo form_error('etunimi'); ?>
    </div>   
    <div class="form-group">
        <label for="sukunimi">Sukunimi:</label>
        <input type="text" class="form-control" id="sukunimi" name="sukunimi" 
            maxlength="50" value="<?php echo $sukunimi; ?>">
        <?php echo form_error('sukunimi'); ?>
    </div>   
    <div class="form-group">
       <label for="email">Sähköposti:</label>
        <input type="text" class="form-control" id="email" name="email"
            maxlength="100" value="<?php echo $email; ?>">
        <?php echo form_error('email'); ?>
    </div>
    <button type="submit" class="btn btn-primary">Tallenna</button>
    <a class="btn btn-danger" href="<?php echo site_url(); ?>">Takaisin</a>
</form>