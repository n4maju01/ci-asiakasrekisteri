<div class="row">
    <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2">
        <h3><?php echo $otsikko; ?></h3>
        <form role="form" method="post" action="<?php echo site_url() . 'toimenpide/tallenna' ?>">
            <div class="form-group">
               <label for="aika">Aika:</label>
                <input type="text" class="form-control" id="aika" name="aika" 
                       value="<?php echo $aika; ?>">
                <?php //echo form_error('aika'); ?>
            </div>
            <div class="form-group">
               <label for="teksti">Teksti:</label>
                <input type="textarea" class="form-control" rows="4" cols="50" 
                    id="teksti" name="teksti" value="<?php echo $teksti; ?>" autofocus>
                <?php echo form_error('teksti'); ?>
            </div>
            <a class="btn btn-danger" href="<?php echo site_url(); ?>">Etusivulle</a>
            <button type="submit" class="btn btn-primary">Tallenna</button>
        </form>
    </div>
</div>