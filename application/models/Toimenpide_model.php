<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Toimenpide_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    public function hae_kaikki($asiakas_id) {
        $this->db->select('aika, teksti, toimenpide, id, kayttaja.etunimi, kayttaja.sukunimi');
        $this->db->from('toimenpide');
        $this->db->join('kayttaja', 'kayttaja_id = toimenpide.kayttaja_id');
        $this->db->where('asiakas_id', $asiakas);
   
        $kysely = $this->db->get('asiakas');
        return $kysely->result();
    }
    
    public function laske_asiakkaat($etsi="") {     
        if (strlen($etsi) > 0) {
            $this->db->like('sukunimi', $etsi);
            $this->db->or_like('etunimi', $etsi);
            $this->db->or_like('email', $etsi);
        }
        
        return $this->db->count_all_results('asiakas');
    }
    
    public function hae($id) {
        $this->db->where('id', $id);
        $kysely = $this->db->get('asiakas');
        return $kysely->row();
    }
    
    public function lisaa($data) {
        $this->db->insert('toimenpide', $data);
        return $this->db->insert_id();
    }
    
    public function muokkaa($data) {
        $this->db->where('id', $data['id']);
        $this->db->update('asiakas', $data);
    }
    
    public function poista($id) {
        $this->db->where('id', $id);
        $this->db->delete('asiakas');
    }
}