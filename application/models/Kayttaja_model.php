<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kayttaja_model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->load->library('encryption');
    }
    
    public function hae_kaikki() {
        $kysely = $this->db->get('kayttaja');
        return $kysely->result();
    }
    
    public function hae($id) {
        $this->db->where('id', $id);
        $kysely = $this->db->get('kayttaja');
        return $kysely->row();
    }
    
    public function lisaa($data) {
        $data['salasana']=$this->encryption->encrypt($data['salasana']);
        $this->db->insert('kayttaja', $data);
        return $this->db->insert_id();
    }
    
    public function muokkaa($data) {
        $data['salasana']->encryption->encrypt($data['salasana']);
        $this->db->where('id', $data['id']);
        $this->db->update('kayttaja', $data);
    }
    
    public function poista($id) {
        $this->db->where('id', $id);
        $this->db->delete('kayttaja');
    }
}