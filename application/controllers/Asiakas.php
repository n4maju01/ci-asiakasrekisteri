<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Asiakas extends CI_Controller {
    public function __construct() {
        parent::__construct();
        
//        if ($this->session->has_userdata('kayttaja') == FALSE) {
//            redirect('kayttaja/kirjaudu');
//        }
        
        $this->load->model('Asiakas_model');
        $this->load->library('form_validation');
        $this->load->library('pagination');
        $this->form_validation->set_error_delimiters("<div class='label label-warning'>", "</div>");
    }
    
    public function index() {
        $etsi = "";
        $asiakkaita_sivulla = 3;
        $jarjestys = "";
        
        if ($this->input->post('etsi')) {
            $etsi = $this->input->post('etsi');
        }
        
        if ($this->session->userdata("jarjestys")) {
            $jarjestys = $this->session->userdata("jarjestys");
        }
        
        if ($this->uri->segment(4)) {
            $jarjestys = $this->uri->segment(4);
            $this->session->set_userdata("jarjestys", $jarjestys);
        }
          
        $data['asiakkaat'] = $this->Asiakas_model->hae_kaikki($etsi, 
                $asiakkaita_sivulla, $this->uri->segment(3), $jarjestys);
        
        $data['sivutuksen_kohta'] = 0;
        if ($this->uri->segment(3)) {
            $data['sivutuksen_kohta'] = $this->uri->segment(3);
        }
        
        $config['base_url'] = site_url('asiakas/index');
        $config['total_rows'] = $this->Asiakas_model->laske_asiakkaat($etsi);
        //$config['total_rows'] = count($data['asiakkaat']);
        $config['per_page'] = $asiakkaita_sivulla;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);
        
        $data['etsi'] = $etsi;
        $data['otsikko'] = 'Asiakasrekisteri';
        $data['sisalto'] = 'asiakas/asiakkaat_view';
        $this->load->view('template', $data);
    }
    
    public function lisaa() {
        $data = array(
            'id'        => '',
            'etunimi'   => '',
            'sukunimi'  => '',
            'email'     => ''
        );
        
        $data['otsikko'] = 'Lisää uusi asiakas';
        $data['sisalto'] = 'asiakas/asiakas_view';
        $this->load->view('template', $data);
    }
    
    public function tallenna() {
        $data = array(
            'id'        => $this->input->post('id'),
            'etunimi'   => $this->input->post('etunimi'),
            'sukunimi'  => $this->input->post('sukunimi'),
            'email'     => $this->input->post('email')
        );
        
        $this->form_validation->set_rules("etunimi", "Etunimi", 
            "required|max_length[50]");
        $this->form_validation->set_rules("sukunimi", "Sukunimi", 
            "required|max_length[50]");
        $this->form_validation->set_rules("email", "Sähköposti", 
            "required|valid_email|is_unique[asiakas.email]|max_length[100]");
        
        if ($this ->form_validation->run() === TRUE) {          
            if (strlen($this->input->post("id")) > 0) {
                $this->Asiakas_model->muokkaa($data);
            }

            else {
                $this->Asiakas_model->lisaa($data);   
            }

            redirect(site_url(), 'refresh');
        }
        
        else {
            $data['otsikko'] = 'Tarkasta asiakas';
            $data['sisalto'] = 'asiakas/asiakas_view';
            $this->load->view('template', $data);
        }
    }
    
    public function varmistus($id) {
        $asiakas = $this->Asiakas_model->hae($id);
        
        $data['otsikko'] = "Poista asiakas";
        $data['kysymys'] = "Haluatko varmasti poistaa asiakkaan " 
            . $asiakas->sukunimi . ", " . $asiakas->etunimi . "?";
        $data['peruuta_osoite'] = site_url();
        $data['ok_osoite'] = site_url() . "asiakas/poista/$id"; 
        $data['sisalto'] = "asiakas/varmistus_view";
        $this->load->view('template', $data);
    }
    
    public function poista($id) {
        $this->Asiakas_model->poista(intval($id));
        redirect(site_url(), 'refresh');
    }
    
    public function muokkaa($id) {
        $asiakas = $this->Asiakas_model->hae($id);
        
        if ($asiakas) {
            $data = array(
                'id'        => $asiakas->id,
                'etunimi'   => $asiakas->etunimi,
                'sukunimi'  => $asiakas->sukunimi,
                'email'     => $asiakas->email
            );

            $data['otsikko'] = 'Muokkaa asiakkaan tietoja';
            $data['sisalto'] = 'asiakas/asiakas_view';
            $this->load->view('template', $data);
        }
        
        else {
            throw new Exception("Asiakasta ei löydy rekisteristä!");
        }
    }
    
    public function suorita() {
        $valinnat = $this->input->post('valinta');
        
        if (!empty ($valinnat)) {
            foreach ($valinnat as $valinta) {
                $this->Asiakas_model->poista($valinta);
            }
        }

        redirect('asiakas/index', 'refresh');
    }
}