<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Toimenpide extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('Toimenpide_model');
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters("<div class='label label-warning'>", "</div>");
    }
    
    public function index() {
        $this->lisaa($id);
    }
    
    public function lisaa($id) {
        $data = array(
            'id'        => '',
            'teksti'   => '',
            'aika'  => date('d.m.Y.H.i')
        );
        
        $asiakas = $this->Asiakas_model->hae($id);
        
        $data['asiakas_nimi'] = $asiakas->nimi;
        $data['otsikko'] = 'Lisää toimenpide';
        $data['sisalto'] = 'toimenpide/toimenpide_view';
        $this->load->view('template', $data);
    }
    
    public function tallenna() {
        $data = array(
            'aika'      => $this->input->post('aika'),
            'teksti'    => $this->input->post('teksti')
        );

        //$this->form_validation->set_rules("aika", "Aika", "required|);
                
        $this->form_validation->set_rules("teksti", "Teksti", "required");
        
        if ($this ->form_validation->run() === TRUE) {          
            $this->Toimenpide_model->lisaa($data);   

            redirect('toimenpide/index', 'refresh');
        }
        
        else {
            
            $data['sisalto'] = 'toimenpide/toimenpide_view';
            $this->load->view('template', $data);
        }
    }
    
    
    public function ulos() {
        $this->session->sess_destroy();
        redirect('kayttaja/kirjaudu', 'refresh');
    }
}