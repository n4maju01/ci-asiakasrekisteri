<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kayttaja extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('Kayttaja_model');
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters("<div class='label label-warning'>", "</div>");
    }
    
    public function index() {
        $this->kirjaudu();
    }
    
    public function kirjaudu() {
        $data = array(
            'email'     => '',
            'salasana'  => ''
        );

        $data['sisalto'] = 'kayttaja/kirjaudu_view';
        $this->load->view('template', $data);
    }
    
    public function rekisteroidy() {
        $data = array(
            'email'     => '',
            'salasana'  => ''
        );

        $data['sisalto'] = 'kayttaja/rekisteroidy_view';
        $this->load->view('template', $data);
    }
    
    public function tallenna() {
        $data = array(
            'email'     => $this->input->post('email'),
            'salasana'  => $this->input->post('salasana')
        );

        $this->form_validation->set_rules("email", "Sähköposti", 
            "required|valid_email|is_unique[kayttaja.email]|max_length[100]");
                
        $this->form_validation->set_rules("salasana", "Salasana", 
            "required|max_length[25]|min_length[8]");
        
        if ($this ->form_validation->run() === TRUE) {          
            $this->Kayttaja_model->lisaa($data);   

            redirect('kayttaja/kirjaudu', 'refresh');
        }
        
        else {
            
            $data['sisalto'] = 'kayttaja/rekisteroidy_view';
            $this->load->view('template', $data);
        }
    }
    
    public function tarkasta() {
        $email = $this->input->post('email');
        $salasana = $this->input->post('salasana');
        
        $this->form_validation->set_rules("email", "Sähköposti", 
            "required|valid_email|max_length[100]");
                
        $this->form_validation->set_rules("salasana", "Salasana", 
            "required|max_length[25]|min_length[8]");
        
        if ($this ->form_validation->run() === TRUE) {
            $kayttaja = $this->Kayttaja_model->
                hae_sahkopostilla_ja_salasanalla($email, $salasana);
            
            if ($kayttaja) {
                $this->session->set_userdata('kayttaja', $kayttaja);
                redirect('asiakas/index');
            }
            
            else {
                redirect('kayttaja/kirjaudu');
            }
        }
   
        else {
            $data = array (
                'email'     => $email,
                'salasana'  => $salasana
            );
            
            $data['sisalto'] = 'kayttaja/kirjaudu_view';
            $this->load->view('template', $data);
        }
    }
    
    public function ulos() {
        $this->session->sess_destroy();
        redirect('kayttaja/kirjaudu', 'refresh');
    }
}