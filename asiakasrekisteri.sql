drop database if exists n4maju01_ci_asiakasrekisteri;
create database n4maju01_ci_asiakasrekisteri;
use n4maju01_ci_asiakasrekisteri;

create table if not exists kayttaja (
    id int primary key auto_increment,
    email varchar(100) unique not null,
    salasana varchar(255) not null,
    tallennettu timestamp default current_timestamp   
) engine = innodb;

create table if not exists asiakas (
    id int primary key auto_increment,
    etunimi varchar(50) not null,
    sukunimi varchar(50) not null,
    email varchar(100) unique not null,
    tallennettu timestamp default current_timestamp
) engine = innodb;

create table if not exists toimenpide (
    id int primary key auto_increment,
    teksti text not null,
    aika datetime,
    tallennettu timestamp default current_timestamp on update current_timestamp,
    kayttaja_id int not null,
    index (kayttaja_id),
    foreign key (kayttaja_id) references kayttaja(id) on delete restrict,
    asiakas_id int not null,
    index (asiakas_id),
    foreign key (asiakas_id) references asiakas(id) on delete restrict
) engine = innodb;

insert into asiakas(etunimi,sukunimi, email)
values ('Jussi','Mannermaa', 'n4maju01@students.oamk.fi');