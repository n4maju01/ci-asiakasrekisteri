$(function() {
   $('#search').keypress(function(e) {
        if (e.which === 13) {
           e.preventDefault();
           var etsi = $('#search').val();
           $('#etsi').val(etsi);
           $('#etsi_lomake').submit();
       }
   });
});